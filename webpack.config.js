var Encore = require('@symfony/webpack-encore');

Encore
    // the project directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // the public path used by the web server to access the previous directory
    .setPublicPath('/build')
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    // uncomment to create hashed filenames (e.g. app.abc123.css)
    // .enableVersioning(Encore.isProduction())


    // JS
     .addEntry('js/app', './assets/js/app.js')
     .addEntry('js/jquery.min', './assets/js/jquery.min.js')
     .addEntry('js/jquery.slim.min', './assets/js/jquery.slim.min.js')
     .addEntry('js/bootstrap.bundle', './assets/js/bootstrap.bundle.min.js')
     .addEntry('js/jqBootstrapValidation.min', './assets/js/jqBootstrapValidation.min.js')
     .addEntry('js/jquery.easing.compatibility', './assets/js/jquery.easing.compatibility.js')
     .addEntry('js/jquery.easing.min', './assets/js/jquery.easing.min.js')
     .addEntry('js/agency.min', './assets/js/agency.min.js')

    // CSS
    .addStyleEntry('css/app', './assets/css/app.scss')
    .addStyleEntry('css/font-awesome', './vendor/font-awesome/css/font-awesome.min.css')

    // uncomment if you use Sass/SCSS files
    .enableSassLoader(function(sassOptions) {},
        {resolveUrlLoader: false
     })
    // uncomment for legacy applications that require $/jQuery as a global variable
     .autoProvidejQuery()
;

module.exports = Encore.getWebpackConfig();

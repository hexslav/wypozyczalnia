const auto = document.getElementById('table');

if(auto){
    auto.addEventListener('click',e=>{

        if(e.target.className === 'btn btn-danger delete-auto'){
            if(confirm('Jesteś pewny że chcesz usunąć to auto z bazy danych?')){
                const id = e.target.getAttribute('data-id');

                fetch('/flota/delete/${id}',{
                    method: 'DELETE'
                }).then(res => window.location.reload());

            }
        }
    });
}
<?php


namespace App\Repository;
use App\Entity\Auto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
/**
 * @method Auto|null find($id, $lockMode = null, $lockVersion = null)
 * @method Auto|null findOneBy(array $criteria, array $orderBy = null)
 * @method Auto[]    findAll()
 * @method Auto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AutoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Auto::class);
    }
    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('a')
            ->where('a.something = :value')->setParameter('value', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */



    public function getKlase($klasa){


        $qb = $this->createQueryBuilder('p')
            ->select('p')
            ->where('p.klasa = :klasa')
            ->setParameter('klasa', $klasa)
            ->orderBy('p.cat', 'ASC');

        return $qb->getQuery()
            ->getResult();

    }



    public function getOsobowe(){


        $qb = $this->createQueryBuilder('p')
            ->select('p')
            ->where('p.cat = :cat')
            ->setParameter('cat', 'Osobowe')
            ->orderBy('p.cat', 'ASC');

        return $qb->getQuery()
            ->getResult();

    }

    public function getAutomaty(){


        $qb = $this->createQueryBuilder('p')
            ->select('p')
            ->where('p.cat = :cat')
            ->setParameter('cat', 'Automaty')
            ->orderBy('p.cat', 'ASC');

        return $qb->getQuery()
            ->getResult();

    }

    public function getDostawcze(){


        $qb = $this->createQueryBuilder('p')
            ->select('p')
            ->where('p.cat = :cat')
            ->setParameter('cat', 'Dostawcze')
            ->orderBy('p.cat', 'ASC');

        return $qb->getQuery()
            ->getResult();

    }

    public function getVan(){


        $qb = $this->createQueryBuilder('p')
            ->select('p')
            ->where('p.cat = :cat')
            ->setParameter('cat', 'Van')
            ->orderBy('p.cat', 'ASC');

        return $qb->getQuery()
            ->getResult();

    }
}
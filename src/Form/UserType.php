<?php
/**
 * Created by PhpStorm.
 * User: hexslav
 * Date: 16.06.18
 * Time: 09:26
 */

namespace App\Form;


use Doctrine\DBAL\Types\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
        ->add('username', TextType::class)
        ->add('email', EmailType::class)
        ->add('password', RepeatedType::class)
        ->add('submit', SubmitType::class, [
            'attr' => [
                'class' =>'btn btn-success pull-right'
            ]
        ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class
        ]);

    }


}
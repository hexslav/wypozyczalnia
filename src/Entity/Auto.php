<?php
/**
 * Created by PhpStorm.
 * User: hexslav
 * Date: 31.05.18
 * Time: 11:56
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AutoRepository")
 */
class Auto
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }



    /**
     * @ORM\Column(type="string", length=255)
     */
    private $cat;

    /**
     * @return mixed
     */
    public function getCat()
    {
        return $this->cat;
    }

    /**
     * @param mixed $cat
     */
    public function setCat($cat)
    {
        $this->cat = $cat;
    }



    /**
     * @ORM\Column(type="string", length=55)
     */
    private $klasa;

    /**
     * @return mixed
     */
    public function getklasa()
    {
        return $this->klasa;
    }

    /**
     * @param mixed $klasa
     */
    public function setklasa($klasa)
    {
        $this->klasa = $klasa;
    }




    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }





    /**
     * @ORM\Column(type="string", length=255)
     */
    private $descri;

    /**
     * @return mixed
     */
    public function getDescri()
    {
        return $this->descri;
    }

    /**
     * @param mixed $descri
     */
    public function setDescri($descri)
    {
        $this->descri = $descri;
    }



    /**
     * @ORM\Column(type="string", length=255)
     */
    private $descrilong;
    /**
     * @return mixed
     */
    public function getDescrilong()
    {
        return $this->descrilong;
    }

    /**
     * @param mixed $descri
     */
    public function setDescrilong($descrilong)
    {
        $this->descrilong = $descrilong;
    }





    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank(message="Please, upload the product image as a img file.")
     *
     * @Assert\Image()
     *
     */

//    /**
//     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Gallery")
//     * @ORM\JoinColumns({
//     *   @ORM\JoinColumn(name="image", referencedColumnName="id")
//     * })
//     */
    private $image;

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }






}

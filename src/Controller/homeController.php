<?php
/**
 * Created by PhpStorm.
 * User: hexslav
 */


namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Auto;


class homeController extends Controller
{

    public function index()
    {

        $auto =$this->getDoctrine()->getRepository(Auto::class)->findAll();
        return $this->render('home/index.html.twig', array('Auta'=>$auto));
//
//        return $this->render('home/index.html.twig', [
//            'controller_name' => 'homeController',
//        ]);

    }
}

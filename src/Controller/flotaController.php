<?php
/**
 * Created by PhpStorm.
 * User: hexslav
 * Date: 30.05.18
 * Time: 18:00
 */

namespace App\Controller;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Auto;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class flotaController extends Controller
{

    public function index()
    {
        $auto =$this->getDoctrine()->getRepository(Auto::class)->findAll();
        return $this->render('flota/index.html.twig', array('Auta'=>$auto));
    }



//    public function AplusFilter(){
//
//        $this->addFilter('locale', '\src\AplusFilter');
//
//        $em = $this->getServiceManager()->get('doctrine.entitymanager.orm_default');
//
//        $filter = $em->getFilters()->enable("locale");
//        $filter->setParameter('locale', 'en');
//
//    }


    public function klasa($klasa){


        $entityManager = $this->getDoctrine()->getManager();
        $plopRepository = $entityManager->getRepository(Auto::class);
        $plops = $plopRepository->getKlase($klasa);

        return $this->render('cat/osobowe/index.html.twig', array('Auta'=>$plops));

    }





    public function osobowe(){

        $entityManager = $this->getDoctrine()->getManager();
        $plopRepository = $entityManager->getRepository(Auto::class);
        $plops = $plopRepository->getOsobowe();

        return $this->render('cat/osobowe/index.html.twig', array('Auta'=>$plops));

    }

    public function automaty(){

        $entityManager = $this->getDoctrine()->getManager();
        $plopRepository = $entityManager->getRepository(Auto::class);
        $plops = $plopRepository->getAutomaty();

        return $this->render('cat/automaty/index.html.twig', array('Auta'=>$plops));

    }

    public function dostawcze(){

        $entityManager = $this->getDoctrine()->getManager();
        $plopRepository = $entityManager->getRepository(Auto::class);
        $plops = $plopRepository->getDostawcze();

        return $this->render('cat/dostawcze/index.html.twig', array('Auta'=>$plops));

    }

    public function van(){

        $entityManager = $this->getDoctrine()->getManager();
        $plopRepository = $entityManager->getRepository(Auto::class);
        $plops = $plopRepository->getVan();

        return $this->render('cat/van/index.html.twig', array('Auta'=>$plops));

    }


    public function new(Request $request){

        $auto =new Auto();
        $form = $this->createFormBuilder($auto)
            ->add('name',TextType::class,array('label'=>'Nazwa auta','attr'=>array('class'=>'form-control')))
        ->add('descri',TextareaType::class,array('label'=>'Opis','required'=>false,'attr'=>array('class'=>'form-control')))
            ->add('descrilong',TextareaType::class,array('label'=>'Opis długi','required'=>false,'attr'=>array('class'=>'form-control')))
            ->add('price',TextType::class,array('label'=>'Cena','required'=>true,'attr'=>array('class'=>'form-control')))
            ->add('cat', ChoiceType::class, array(
                'choices'  => array(
                    'Osobowe' => 'Osobowe',
                    'automaty' => 'automaty',
                    'van' => 'van',
                    'Dostawcze' => 'Dostawcze',
                )))
            ->add('klasa', ChoiceType::class, array(
                'choices'  => array(
                    'A' => 'A',
                    'A+' => 'A+',
                    'B' => 'B',
                    'B+' => 'B+',
                )))
            ->add('image',FileType::class,array('label'=>'Zdjecie auta','attr'=>array('class'=>'form-control')))
            ->add('save',SubmitType::class,array('label'=>'Dodaj','attr'=>array('class'=>'btn btn-primary mt-3')))
            ->getForm();

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){

            /**
             * @var UploadedFile $file
             */
            $file=$auto->getImage();
            $fileName = md5(uniqid()).'-'.$file->guessExtension();

            $file->move(
              $this->getParameter('image_directory'),$fileName
            );


            $auto = $form->getData();
            $auto ->setImage($fileName);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager ->persist($auto);
            $entityManager ->flush();

            return $this->redirectToRoute('new');
        }

        $auto =$this->getDoctrine()->getRepository(Auto::class)->findAll();

        return $this->render('new/index.html.twig',array('form'=>$form->createView(),'Auta'=>$auto));



    }

    public function edit(Request $request, $id){

        $auto =new Auto();
        $auto =$this->getDoctrine()->getRepository(Auto::class)->find($id);


        $form = $this->createFormBuilder($auto)
            ->add('name',TextType::class,array('label'=>'Nazwa auta','attr'=>array('class'=>'form-control')))
            ->add('descri',TextareaType::class,array('label'=>'Opis krótki','required'=>false,'attr'=>array('class'=>'form-control')))
            ->add('descrilong',TextareaType::class,array('label'=>'Opis długi','required'=>false,'attr'=>array('class'=>'form-control')))
            ->add('price',TextType::class,array('label'=>'Cena','required'=>true,'attr'=>array('class'=>'form-control')))
            ->add('cat', ChoiceType::class, array(
                'choices'  => array(
                    'Osobowe' => 'Osobowe',
                    'automaty' => 'automaty',
                    'van' => 'van',
                    'Dostawcze' => 'Dostawcze',
                )))
            ->add('klasa', ChoiceType::class, array(
                'choices'  => array(
                    'A' => 'A',
                    'A+' => 'A+',
                    'B' => 'B',
                    'B+' => 'B+',
                )))
            ->add('image', FileType::class,array('label'=>'Zdjecie auta','data_class' => null,'required'=>false, 'attr'=>array('class'=>'form-control')))
            ->add('save',SubmitType::class,array('label'=>'Edytuj','attr'=>array('class'=>'btn btn-primary mt-3')))
            ->getForm();

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){

            /**
             * @var UploadedFile $file
             */
            $file=$auto->getImage();
            $fileName = md5(uniqid()).'-'.$file->guessExtension();

            $file->move(
                $this->getParameter('image_directory'),$fileName
            );



            $auto ->setImage($fileName);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager ->flush();

            return $this->redirectToRoute('new');
        }

        $auto =$this->getDoctrine()->getRepository(Auto::class)->findAll();

        return $this->render('edit/index.html.twig',array('form'=>$form->createView(),'Auta'=>$auto));



    }

    public function save()
    {
        $entityManager = $this->getDoctrine()->getManager();

        $auto = new Auto();


        $auto ->setName("Audi A1");
        $auto ->setPrice("254");
        $auto ->setDescri("Najlepsze auto na świecie bum!");

        $entityManager->persist($auto);
        $entityManager->flush();

        return new Response('Dodano auto o numerze id: '.$auto->getId() );
    }

    public function show($id){
        $auto =$this->getDoctrine()->getRepository(Auto::class)->find($id);
        return $this->render('show/index.html.twig', array('Auta'=>$auto));

    }

    /**
     * @param Request $request
     * @param $id
     */


    public function delete(Request $request, $id){

        $auto =$this->getDoctrine()->getRepository(Auto::class)->find($id);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager ->remove($auto);
        $entityManager ->flush();

        $response = new Response();
        $response->send();




    }






}

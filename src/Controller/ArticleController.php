<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;

class ArticleController{


    /** 
     * @Route("/podstrony/{slug}")
     */

    public function show($slug){
        return new Response(sprintf('Jesteś na:',$slug));
    }

}